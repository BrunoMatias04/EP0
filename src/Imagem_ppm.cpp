#include "Imagem_ppm.hpp"

using namespace std;

Imagem_ppm::Imagem_ppm(){
	setPixels(" ");
	setLargura(0);
	setAltura(0);
	setCor_Max(0);
	setNome_da_Imagem(" ");
	setNumero_magico(" ");
}

Imagem_ppm::Imagem_ppm(string Pixels,int Largura,int Altura,int Cor_Max,string Nome_da_Imagem,string numero_magico){
	setPixels(Pixels);
	setLargura(Largura);
	setAltura(Altura);
	setCor_Max(Cor_Max);
	setNome_da_Imagem(Nome_da_Imagem);
	setNumero_magico(numero_magico);
}
string Imagem_ppm::getPixels()
{
	return Pixels;
}
int Imagem_ppm::getLargura()
{
	return Largura;
}
int Imagem_ppm::getAltura()
{
	return Altura;
}
int Imagem_ppm::getCor_Max()
{
	return Cor_Max;
}
string Imagem_ppm::getNome_da_Imagem()
{
	return Nome_da_Imagem;
}
string Imagem_ppm::getNumero_magico()
{
	return numero_magico;
}
void Imagem_ppm::setPixels(string Pixels)
{
	this->Pixels = Pixels;
}
void Imagem_ppm::setLargura(int Largura)
{
	this->Largura = Largura;
}
void Imagem_ppm::setAltura(int Altura)
{
	this->Altura = Altura;
}
void Imagem_ppm::setCor_Max(int Cor_Max)
{
	this->Cor_Max = Cor_Max;
}
void Imagem_ppm::setNome_da_Imagem(string Nome_da_Imagem)
{
	this->Nome_da_Imagem = Nome_da_Imagem;
}
void Imagem_ppm::setNumero_magico(string numero_magico)
{
	this->numero_magico = numero_magico;
}

void Imagem_ppm::LerImagem()
{
	ifstream  Arquivo_Imagem(getNome_da_Imagem().c_str());
        if (Arquivo_Imagem.is_open())
        {
        cout <<"Arquivo aberto!!"<< endl;
        }
        else
        {
        cout <<"ERRO , Nome do arquivo invalido" << endl;
        exit(EXIT_FAILURE);
        }	
	cout << "Lendo Imagem...!!" << endl;
	string lixo,pixels_conteudo,N_magico;
	char pixel;
	getline(Arquivo_Imagem,N_magico);
	setNumero_magico(N_magico);
	if (N_magico != "P6")
	{
	cout << "Imagem não é ppm!!" << endl;
	exit(EXIT_FAILURE);
	}
	getline(Arquivo_Imagem,lixo);
	int inteiro;
	if(lixo[0] != '#')
	{
	int num = stoi(lixo);
	setLargura(num);
	}
	else
	{
	Arquivo_Imagem >> inteiro;
        setLargura(inteiro);
	}

        Arquivo_Imagem >> inteiro;
        setAltura(inteiro);

        Arquivo_Imagem >> inteiro;
        setCor_Max(inteiro);

	getline(Arquivo_Imagem,pixels_conteudo);
	
	while (!Arquivo_Imagem.eof())
	{
	Arquivo_Imagem.get(pixel);
	pixels_conteudo += pixel;
	}	
	setPixels(pixels_conteudo);
	Arquivo_Imagem.close();
}
void Imagem_ppm::imprimeTudo(){
	cout <<"Numero Magico: "<< getNumero_magico() << endl;
	cout <<"Largura: "<< getLargura() << endl;
	cout <<"Altura: "<< getAltura() << endl;
	cout <<"Escala Maxima em tela das cores: "<< getCor_Max() << endl;
}
void Imagem_ppm::Salva_pixels_em_outro_arquivo()
{
	ofstream Arquivo_Novo;
	Arquivo_Novo.open("Arquivo_Novo.ppm",ios_base::out);
	Arquivo_Novo << getNumero_magico() << endl;
	Arquivo_Novo << getLargura() << endl;
	Arquivo_Novo << getAltura() << endl;
	Arquivo_Novo << getCor_Max() << endl;
	Arquivo_Novo << getPixels() << endl;
	Arquivo_Novo.close();
}
