#include "Imagem_ppm.hpp"

using namespace std;

int main() {
	Imagem_ppm *imagem = new Imagem_ppm();
	
	string nome_do_arquivo_da_imagem;
	cout << "Local da imagem: " << endl;
	cin >> nome_do_arquivo_da_imagem;
	imagem->setNome_da_Imagem(nome_do_arquivo_da_imagem);


	imagem->LerImagem();
	imagem->imprimeTudo();
	imagem->Salva_pixels_em_outro_arquivo();

	delete(imagem);
	return 0;
}
