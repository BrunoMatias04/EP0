# Orientação a Objetos 1/2016
*obs:O compilador deve suportar a versão 11 do c++ (para rodar a função "stoi").

* Nome : Bruno Matias Casas
* Matricula : 150051212 

## EP0 - C++

* 1 - Abra a imagem no formato PPM
* 2 - Leia o conteúdo da imagem
* 3 - Salve o conteúdo em outro arquivo

* Mais detalhes na wiki [home](https://gitlab.com/OOFGA-2016-1/EP0/wikis/home)
### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
### Ao executar o programa ...

* Digitar o local da imagem ppm desejada(Ex:/home/bmatias/oo2016/EP0/imagens/unb.ppm).
* Dar enter e o programa irá ler a imagem e printar sua largura,altura e a escala maxima das cores.
* Alem disso irá salvar todo conteudo lido em um arquivo chamado "Arquivo_novo.ppm".
