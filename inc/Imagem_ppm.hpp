#ifndef IMAGEM_PPM_HPP
#define IMAGEM_PPM_HPP
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
using namespace std;


class Imagem_ppm{

private:
	string Pixels;
	int Largura;
	int Altura;
	int Cor_Max;
	string Nome_da_Imagem;
	string numero_magico;
public: 
	Imagem_ppm();
	Imagem_ppm(string Pixels,int Largura,int Altura,int Cor_Max,string Nome_da_Imagem,string numero_magico);

	string getPixels();
	int getLargura();
	int getAltura();
	int getCor_Max();
	string getNome_da_Imagem();
	string getNumero_magico();
	
	void setPixels(string Pixels);
	void setLargura(int Largura);
	void setAltura(int Altura);
	void setNome_da_Imagem(string Nome_da_Imagem);
	void setCor_Max(int Cor_Max);
	void setNumero_magico(string numero_magico);
	
	void LerImagem();
	void imprimeTudo();
	void Salva_pixels_em_outro_arquivo();
};
#endif
